// Custom sprite shader - no lighting, on/off alpha
// http://gamasutra.com/blogs/JoshSutphin/20130519/192539/Making_2D_Games_With_Unity.php
Shader "Custom/SimpleSpriteShader" {
Properties {
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
}
SubShader {
    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
//    LOD 100
    ZWrite Off
    Blend SrcAlpha OneMinusSrcAlpha
    Lighting Off
    Pass {
        SetTexture [_MainTex] { combine texture } 
    }
}
}

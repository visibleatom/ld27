
using System.Collections.Generic;
using pax.core; //< Core non-game-specific libraries
using pax.input; //< Core non-game-specific libraries
using pax.physics; //< Core non-game-specific libraries


namespace game {

[System.Serializable]
public class MainGame 
{
    const string LOADING_TO_MAINMENU = "loadingToMainMenu";

    public bool GameAlive;

    private bool mDestroying;
    
    private InputSystem mInputSystem;
    private UserInterface mUserInterface;
    private List<IUpdatable> mSystems;
    private ILogProvider mLogProvider;
    private LevelContainer[] mLevels; 
    private Level mCurrentLevel;
    // I'm assuming that, when using different systems, this quad prefab would be
    // something else altogether (not needed, most likely)
    private UnityEngine.Transform mQuadPrefab;

    public MainGame(UnityEngine.Transform quadPrefab, SpriteAtlasContainer inHeroAtlas, SpriteAtlasContainer inEnemyAtlas, LevelContainer[] inLevels)
    {
        //UnityEngine.Debug.Log("MainGame ctor");
        mQuadPrefab = quadPrefab;
        pax.core.Sprite.SetUnityPrefab(quadPrefab);

        game.Global.HeroAtlas = new SpriteAtlas(inHeroAtlas.mTexture, inHeroAtlas.GetXmlData());
        game.Global.EnemyAtlas = new SpriteAtlas(inEnemyAtlas.mTexture, inEnemyAtlas.GetXmlData());
        game.Global.PhysicsEngine = new PhysicsEngine();
        mLevels = inLevels;
    }

    // Use this for initialization
    public void Start ()
    {

        // Initialize the static values
        GameAlive = true;
        
        // Initialize member values
        mDestroying = false;
        
        // Initialize all our providers
        mLogProvider = new LogProvider();
        mLogProvider.SetLogEnabled(true);
        
        // Initialize everything else.
        InitializeSystems();
    }
    
    // Update is called once per frame
    public  void Update ()
    {
        if (GameAlive)
        {
            int elapsedMs = UnityEngine.Mathf.FloorToInt(UnityEngine.Time.deltaTime * 1000);
            // Read/store the input from the various controller methods
            // We're doing this by having the input system as the very
            // first system in the systems list to process in UpdateSystems()
            // ReadInput(elapsedMs); 
            
            // Update the various variables/states of the game.
            UpdateSystems(elapsedMs);
            
            // Render the game based on the current state.
            // This sort of thing is not needed in Unity
            //Render();          
        }
        else if (!mDestroying)
        {
            mDestroying = true;
            ShutdownSystems();
        }
    }

    public void InitializeSystems()
    {
        mSystems = new List<IUpdatable>();
        
        // The input system HAS to be the first IUpdatable system added.
        // At the very least, it must come before any system that would
        // make use of inputs active during this frame.
        mInputSystem = new InputSystem();
        mInputSystem.SetLogProvider(mLogProvider);
        // TODO - this will need to be either registering the input system
        // as the desired service from here on out, or else get ready to pass
        // in mInputSystem to any subsystem that might need it.
        game.Global.Input = mInputSystem;

        //mUserInterface = new UserInterface();
        //mUserInterface.SetLogProvider(mLogProvider);
        // TODO - also, registering somewhere better.
        //game.Global.UserInterface = mUserInterface;

        mSystems.Add(mInputSystem);
        //mSystems.Add(mUserInterface);
        // Sound
        // UI
        //mSystems.Add(mGameStateManager);

        mCurrentLevel = new Level();
        LevelContainer tmpContainer = mLevels[0];
        mCurrentLevel.LoadLevelData(tmpContainer.mAtlasTexture, tmpContainer.GetAtlasDataXml(), tmpContainer.mLevelJson.text);
        mSystems.Add(mCurrentLevel);
    }

    public void ShutdownSystems()
    {
        foreach(IUpdatable system in mSystems)
        {
            system.Shutdown();
        }
        mSystems.Clear();
    }

    public void UpdateSystems (int elapsedMs)
    {
        // This is assuming the foreach will loop in a consistent order. 
        // FIFO sort of order. Otherwise, we should use an index-based for loop.
        foreach(IUpdatable system in mSystems)
        {
            // Log("Updating System: " + system.ToString());
            system.Update(elapsedMs);
        }
    }
    
}
}
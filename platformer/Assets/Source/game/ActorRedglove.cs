using UnityEngine;
using System.Collections;
namespace game {

public class ActorRedglove : Actor
{
    const float WALK_SPEED = 32f;
    const float RUN_SPEED = 96f;
    const float AIR_FAST_SPEED = 96f;
    const float AIR_SLOW_SPEED = 32f;
    
    const float JUMP_FORCE = 150f;
    const float TIME_JUMP_FORCE_MS = 200f;
    enum State
    {
        IDLE,
        WALK_RIGHT,
        WALK_LEFT,
        RUN_RIGHT,
        RUN_LEFT,
        JUMP_UP,
        JUMP_APEX,
        JUMP_DOWN,
    };

    private State mState;
    private int mTimeInState;
    private pax.math.Vector2 mJumpVector;


    public ActorRedglove(float width, float height) : base(width, height)
    {
        mPhysicsBody.Id = "ActorRedglove";
        mTimeInState = 0;
        mJumpVector = new pax.math.Vector2(0,0);
        ArrayList animFrames;

        animFrames = new ArrayList();
        animFrames.Add("idle_001");
        mAnim.AddAnimation("none", animFrames );

        animFrames = new ArrayList();
        animFrames.Add("idle_001");
        animFrames.Add("idle_002");
        animFrames.Add("idle_003");
        animFrames.Add("idle_004");
        animFrames.Add("idle_005");
        animFrames.Add("idle_006");
        animFrames.Add("idle_007");
        animFrames.Add("idle_008");
        animFrames.Add("idle_009");
        animFrames.Add("idle_010");
        animFrames.Add("idle_011");
        animFrames.Add("idle_012");
        animFrames.Add("idle_013");
        animFrames.Add("idle_014");
        animFrames.Add("idle_015");
        animFrames.Add("idle_016");
        animFrames.Add("idle_017");
        animFrames.Add("idle_018");
        animFrames.Add("idle_019");
        animFrames.Add("idle_020");
        animFrames.Add("idle_021");
        animFrames.Add("idle_022");
        animFrames.Add("idle_023");
        animFrames.Add("idle_024");
        animFrames.Add("idle_025");
        animFrames.Add("idle_026");
        animFrames.Add("idle_027");
        animFrames.Add("idle_028");
        animFrames.Add("idle_029");
        animFrames.Add("idle_030");
        animFrames.Add("idle_031");
        mAnim.AddAnimation("idle", animFrames );
        
        animFrames = new ArrayList();
        animFrames.Add("RUN_MAN_RUN_001");
        animFrames.Add("RUN_MAN_RUN_002");
        animFrames.Add("RUN_MAN_RUN_003");
        animFrames.Add("RUN_MAN_RUN_004");
        animFrames.Add("RUN_MAN_RUN_005");
        animFrames.Add("RUN_MAN_RUN_006");
        animFrames.Add("RUN_MAN_RUN_007");
        animFrames.Add("RUN_MAN_RUN_008");
        animFrames.Add("RUN_MAN_RUN_009");
        animFrames.Add("RUN_MAN_RUN_010");
        animFrames.Add("RUN_MAN_RUN_011");
        animFrames.Add("RUN_MAN_RUN_012");
        animFrames.Add("RUN_MAN_RUN_013");
        animFrames.Add("RUN_MAN_RUN_014");
        animFrames.Add("RUN_MAN_RUN_015");
        animFrames.Add("RUN_MAN_RUN_016");
        animFrames.Add("RUN_MAN_RUN_017");
        animFrames.Add("RUN_MAN_RUN_018");
        animFrames.Add("RUN_MAN_RUN_019");
        animFrames.Add("RUN_MAN_RUN_020");
        animFrames.Add("RUN_MAN_RUN_021");
        animFrames.Add("RUN_MAN_RUN_022");
        animFrames.Add("RUN_MAN_RUN_023");
        animFrames.Add("RUN_MAN_RUN_024");
        animFrames.Add("RUN_MAN_RUN_025");
        animFrames.Add("RUN_MAN_RUN_026");
        animFrames.Add("RUN_MAN_RUN_027");
        animFrames.Add("RUN_MAN_RUN_028");
        animFrames.Add("RUN_MAN_RUN_029");
        animFrames.Add("RUN_MAN_RUN_030");
        animFrames.Add("RUN_MAN_RUN_031");        
        mAnim.AddAnimation("run", animFrames );

        animFrames = new ArrayList();
        animFrames.Add("death_simple_001");
        animFrames.Add("death_simple_002");
        animFrames.Add("death_simple_003");
        animFrames.Add("death_simple_004");
        animFrames.Add("death_simple_005");
        animFrames.Add("death_simple_006");
        animFrames.Add("death_simple_007");
        animFrames.Add("death_simple_008");
        animFrames.Add("death_simple_009");
        animFrames.Add("death_simple_010");
        animFrames.Add("death_simple_011");
        animFrames.Add("death_simple_012");
        animFrames.Add("death_simple_013");
        animFrames.Add("death_simple_014");
        animFrames.Add("death_simple_015");
        animFrames.Add("death_simple_016");        
        mAnim.AddAnimation("death", animFrames );

        animFrames = new ArrayList();
        animFrames.Add("jump_001");
        animFrames.Add("jump_002");
        animFrames.Add("jump_003");
        animFrames.Add("jump_004");
        animFrames.Add("jump_005");
        animFrames.Add("jump_006");
        animFrames.Add("jump_007");
        animFrames.Add("jump_008");
        animFrames.Add("jump_009");
        animFrames.Add("jump_010");
        animFrames.Add("jump_011");
        animFrames.Add("jump_012");
        animFrames.Add("jump_013");
        animFrames.Add("jump_014");
        animFrames.Add("jump_015");
        animFrames.Add("jump_016");
        animFrames.Add("jump_017");
        animFrames.Add("jump_018");
        animFrames.Add("jump_019");
        animFrames.Add("jump_020");
        animFrames.Add("jump_021");
        animFrames.Add("jump_022");
        animFrames.Add("jump_023");
        animFrames.Add("jump_024");
        animFrames.Add("jump_025");
        animFrames.Add("jump_026");
        animFrames.Add("jump_027");
        animFrames.Add("jump_028");
        animFrames.Add("jump_029");
        mAnim.AddAnimation("jump", animFrames ); //< I need to split this into jump-start, jump-hang, and jump-end
        
        animFrames = new ArrayList();
        animFrames.Add("walk_001");
        animFrames.Add("walk_002");
        animFrames.Add("walk_003");
        animFrames.Add("walk_004");
        animFrames.Add("walk_005");
        animFrames.Add("walk_006");
        animFrames.Add("walk_007");
        animFrames.Add("walk_008");
        animFrames.Add("walk_009");
        animFrames.Add("walk_010");
        animFrames.Add("walk_011");
        animFrames.Add("walk_012");
        animFrames.Add("walk_013");
        animFrames.Add("walk_014");
        animFrames.Add("walk_015");
        animFrames.Add("walk_016");
        animFrames.Add("walk_017");
        animFrames.Add("walk_018");
        animFrames.Add("walk_019");
        animFrames.Add("walk_020");
        animFrames.Add("walk_021");
        animFrames.Add("walk_022");
        animFrames.Add("walk_023");
        animFrames.Add("walk_024");
        animFrames.Add("walk_025");
        animFrames.Add("walk_026");
        animFrames.Add("walk_027");
        animFrames.Add("walk_028");
        animFrames.Add("walk_029");
        animFrames.Add("walk_030");
        animFrames.Add("walk_031");
        mAnim.AddAnimation("walk", animFrames ); 
        
        mSprite.SetAtlas(game.Global.HeroAtlas);

        mAnim.SetDefaultAnimation("none");
        mAnim.StartAnimation("idle");

    }

    public void GoLeft(bool running)
    {
        // Are we in mid-air (and thus have limited control)
        // or are we on the ground?

        if (IsOnGround())
        {
            mSprite.Flip(true, false);
            if (running)
            {
                ChangeState(State.RUN_LEFT);
                mAnim.StartAnimation("run", -1, 60);
                mPhysicsBody.SetSpeedX(-RUN_SPEED);
            }
            else
            {
                ChangeState(State.WALK_LEFT);
                mAnim.StartAnimation("walk");
                mPhysicsBody.SetSpeedX(-WALK_SPEED);
            }
        }
        else
        {
            if (running)
            {
                mPhysicsBody.SetSpeedX(-AIR_FAST_SPEED);
            }
            else
            {
                mPhysicsBody.SetSpeedX(-AIR_SLOW_SPEED);
            }
        }        
    }


    public void GoRight(bool running)
    {
        // Are we in mid-air (and thus have limited control)
        // or are we on the ground?
        if (IsOnGround())
        {
            mSprite.Flip(false, false);
            if (running)
            {
                ChangeState(State.RUN_RIGHT);
                mAnim.StartAnimation("run", -1, 60);
                mPhysicsBody.SetSpeedX(RUN_SPEED);
            }
            else
            {
                ChangeState(State.WALK_RIGHT);
                mAnim.StartAnimation("walk");
                mPhysicsBody.SetSpeedX(WALK_SPEED);
            }
        }
        else
        {
            if (running)
            {
                mPhysicsBody.SetSpeedX(AIR_FAST_SPEED);
            }
            else
            {
                mPhysicsBody.SetSpeedX(AIR_SLOW_SPEED);
            }
        }
    }

    public void StopWalking()
    {
        if (IsOnGround())
        {
            ChangeState(State.IDLE);
            mAnim.StartAnimation("idle");
            mPhysicsBody.SetSpeedX(0f);
        }
    }

    public void Jump()
    {
        if (CanJump())
        {
            ChangeState(State.JUMP_UP);
            mJumpVector.X = 0f;
            mJumpVector.Y = JUMP_FORCE * 3;
            mPhysicsBody.AddForce("heroJump", mJumpVector);
            //mPhysicsBody.SetSpeedY(-JUMP_FORCE);
        }
    }

    private bool CanJump()
    {
        return mState != State.JUMP_UP && mState != State.JUMP_APEX && mState != State.JUMP_DOWN;
    }

    private bool IsOnGround()
    {
        return mState == State.IDLE 
            || mState == State.RUN_LEFT || mState == State.WALK_LEFT
            || mState == State.RUN_RIGHT || mState == State.WALK_RIGHT;
    }
    
    public override void Update(int elapsedMs)
    {
        mTimeInState += elapsedMs;
        // ASSUMING that we are not falling...
        switch(mState)
        {
            case State.JUMP_UP:
                if (mTimeInState > TIME_JUMP_FORCE_MS)
                {
                    mJumpVector.Y = 0;
                    ChangeState(State.JUMP_APEX);
                }
                mPhysicsBody.AddForce("heroJump", mJumpVector);
            break;
            case State.JUMP_APEX:
                if (mTimeInState > 1000)
                {
                    mAnim.StartAnimation("idle");
                    ChangeState(State.IDLE);
                }
            break;
            case State.JUMP_DOWN:

            break;
            case State.IDLE:
            break;
            case State.WALK_RIGHT:
                //mPhysicsBody.X += walkSpeedPerMs * (float)elapsedMs;
            break;
            case State.WALK_LEFT:
                //mPhysicsBody.X -= walkSpeedPerMs * (float)elapsedMs;
            break;
            case State.RUN_RIGHT:
                //mPhysicsBody.X += runSpeedPerMs * (float)elapsedMs;
            break;
            case State.RUN_LEFT:
                //mPhysicsBody.X -= runSpeedPerMs * (float)elapsedMs;
            break;
        }

        base.Update(elapsedMs);

    }
    protected override void UpdatePhysics(int elapsedMs)
    {

    }


    private void ChangeState(State newState)
    {
        mState = newState;
        mTimeInState = 0;
    }



}

}
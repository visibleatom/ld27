using UnityEngine;
using System.Collections;

public class InputCodes
{
	// ingame? codes
    public const string ACTION_CLICK = "actionClick";
    public const string JUMP = "jump";

    // UI? codes
    public const string DEFAULT_ADVANCE = "defaultadvance";
    public const string CONTEXT_MENU = "contextMenu";
    public const string DIRECTION_RIGHT = "dirRight";
    public const string DIRECTION_LEFT = "dirLeft";
    public const string RUN_MODIFIER = "runModifier";
}

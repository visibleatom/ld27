using pax.core;
using pax.input;
using System.Collections.Generic;
using System.Collections;

namespace game {
public class HeroController : IUpdatable
{

    // Yeah, for now, we know that the hero is always
    // the redglove
    ActorRedglove mHero;

    public HeroController()
    {
        //UnityEngine.Debug.Log("HeroController ctor");
        game.Global.Input.AddSynonymKeyCode(InputCodes.JUMP, UnityEngine.KeyCode.Space);
        game.Global.Input.AddSynonymKeyCode(InputCodes.DIRECTION_RIGHT, UnityEngine.KeyCode.RightArrow);
        game.Global.Input.AddSynonymKeyCode(InputCodes.DIRECTION_RIGHT, UnityEngine.KeyCode.D);
        game.Global.Input.AddSynonymKeyCode(InputCodes.DIRECTION_LEFT, UnityEngine.KeyCode.LeftArrow);
        game.Global.Input.AddSynonymKeyCode(InputCodes.DIRECTION_LEFT, UnityEngine.KeyCode.A);
        game.Global.Input.AddSynonymKeyCode(InputCodes.RUN_MODIFIER, UnityEngine.KeyCode.LeftShift);
        game.Global.Input.AddSynonymKeyCode(InputCodes.RUN_MODIFIER, UnityEngine.KeyCode.RightShift);
    }

    public void SetHero(ActorRedglove hero)
    {
        mHero = hero;
    }

    public void SetLogProvider(ILogProvider provider)
    {

    }

    #region IUpdatable implementation
    public void Update (int elapsedMs)
    {
        if (mHero == null) return;

        bool walkRight = false;
        bool walkLeft = false;
        bool justStoppedWalking = false;
        InputInfo inputInfo;
        inputInfo = game.Global.Input.GetInputInfo(InputCodes.DIRECTION_RIGHT);
        if (inputInfo.Active)
        {
            walkRight = true;
        }
        else if (inputInfo.JustReleased)
        {
            walkRight = false;
            justStoppedWalking = true;
        }
        inputInfo = game.Global.Input.GetInputInfo(InputCodes.DIRECTION_LEFT);
        if (inputInfo.Active)
        {
            walkLeft = true;
        }
        else if (inputInfo.JustReleased)
        {
            walkLeft = false;
            justStoppedWalking = true;
        }        

        bool runModifier = game.Global.Input.GetInputInfo(InputCodes.RUN_MODIFIER).Active;

        if (walkRight)
        {
            mHero.GoRight(!runModifier);
        }
        else if (walkLeft)
        {
            mHero.GoLeft(!runModifier);
        }
        else if (justStoppedWalking)
        {
            mHero.StopWalking();
        }

        if (game.Global.Input.GetInputInfo(InputCodes.JUMP).JustPressed)
        {
            mHero.Jump();
        }

    }

    public void Shutdown ()
    {
        //throw new System.NotImplementedException ();
    }
    #endregion
    
}
}
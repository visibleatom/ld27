using UnityEngine;
using System.Collections;
using pax.core;
using pax.physics;
using pax.math;

namespace game {

public class Actor : IUpdatable
{

    public float X { get { return mPhysicsBody.X; }  set { mPhysicsBody.X = value; }}
    public float Y { get { return mPhysicsBody.Y; }  set { mPhysicsBody.Y = value; }}


    protected Sprite mSprite;
    protected Anim mAnim;
    protected PhysicsBody mPhysicsBody;


    public Actor(float width, float height)
    {
        mSprite = new Sprite(width, height);
        mPhysicsBody = new PhysicsBody(game.Global.PhysicsEngine);
        mPhysicsBody.SetDimensions(new pax.math.Rect(X, Y, width, height));
        mPhysicsBody.Id = "Actor";
        game.Global.PhysicsEngine.RegisterBody(mPhysicsBody);
        mAnim = new Anim();
        // Let anyone who extends this set any
        // custom animations.
    }

    public void AddForce(string id, pax.math.Vector2 force)
    {
        mPhysicsBody.AddForce(id, force);
    }

    public virtual void Update(int elapsedMs)
    {
        mPhysicsBody.Update(elapsedMs);
        mAnim.Update(elapsedMs);

        UpdatePhysics(elapsedMs);

        mSprite.X = mPhysicsBody.X;
        mSprite.Y = mPhysicsBody.Y;
        mSprite.SetFrame(mAnim.GetFrameId());
    }

    protected virtual void UpdatePhysics(int elapsedMs)
    {
        
    }

    public void OnCollide(Actor other)
    {
        
    }

    public void Shutdown()
    {
        //mAnim.Shutdown();
        //mSprite.Shutdown();
    }

}
}
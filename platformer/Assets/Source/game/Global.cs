using UnityEngine;
using System.Collections;

namespace game {

// I consider this a hack class. It's a singleton, container of other not-quite-singletons.
// Any of these values could be null, but in reality I don't expect they'll be.
// HACK
// TODO - remove this class, replace it with a better pattern.
public class Global 
{
    public static pax.input.IInputSystem Input;
    public static game.UserInterface UserInterface;
    public static pax.physics.PhysicsEngine PhysicsEngine;

    // ok this is a horrible hack
    public static pax.core.SpriteAtlas HeroAtlas;
    public static pax.core.SpriteAtlas EnemyAtlas;

    public static pax.core.SpriteAtlas LevelAtlas;
    
}

}
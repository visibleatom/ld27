using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using pax.core;
using pax.physics;
using SimpleJSON;

namespace game {

public class Level : IUpdatable
{
    struct Layer
    {
        public ArrayList actors;
        public ArrayList tiles;
        public string name;
        // todo - collision list of other layer names.
    }

    private Dictionary<string, Layer> mLayers;
    private SpriteAtlas mTextureAtlas;
    private Actor mActorFollow;
    private UnityEngine.Vector3 mCameraFollow;
    private HeroController mHeroController;
    private pax.math.Vector2 mGravity;
    public Level()
    {
        mLayers = new Dictionary<string, Layer>();
        mCameraFollow = Camera.main.transform.position;
        mHeroController = new HeroController();
        mGravity = new pax.math.Vector2(0f, 0f);//-150f);
    }

    // I named the second param jsonData in order to be very explicit as far as what
    // format this function is currently expecting.
    public void LoadLevelData(Texture tileTexture, System.Xml.XmlDocument xmlData, string jsonData)
    {
        // hack
        const int mGlovedManTile = 3;
        const int mTankTile = 1;


        mTextureAtlas = new SpriteAtlas(tileTexture, xmlData);
        // TODO - REALLY need to do a shutdown of all elements, here.
        mLayers.Clear();

        JSONNode jsonNode = JSONNode.Parse(jsonData);
        int numTilesHeight = jsonNode["height"].AsInt;
        int numTilesWidth = jsonNode["width"].AsInt;
        int tileHeight = jsonNode["tileheight"].AsInt;
        int tileWidth = jsonNode["tilewidth"].AsInt;

        // There's only 1 layer for now
        JSONArray layers = jsonNode["layers"].AsArray;
        // All these are property of layers (that we don't care about right now)
        // "height":30,
        // "name":"Tile Layer 1",
        // "opacity":1,
        // "type":"tilelayer",
        // "visible":true,
        // "width":40,
        // "x":0,
        // "y":0
        int numLayers = layers.Count;

        for (int l = 0; l < numLayers; l++)
        {

            JSONArray data = layers[l]["data"].AsArray; //< bytefield for tiles.
            JSONNode properties = layers[l]["properties"];
            bool collidable = (properties == null ? false : properties["playerCollision"].AsBool);
                collidable = true;
            Layer newLayer = new Layer();
            newLayer.tiles = new ArrayList();
            newLayer.actors = new ArrayList();
            newLayer.name = layers[l]["name"];

            int numTiles = data.Count; //< should also match numTilesHeight * numTilesWidth
            int tileType;
            int row = 0;
            int col = 0;
            int tileX = 0;
            int tileY = 0;
            int tileZ = 0;

            for (int i = 0; i < numTiles; i++)
            {
                col++;
                if (col >= numTilesWidth)
                {
                    row++;
                    col = 0;
                }
                tileX = tileWidth * col;
                tileY = tileHeight * (numTilesHeight - row) - (numTilesHeight * tileHeight);
                tileZ = -l;
                tileType = data[i].AsInt;
                if (tileType != 0)
                {
                    switch(tileType)
                    {
                    case mGlovedManTile:
                    {
                        ActorRedglove actor = new ActorRedglove(tileWidth, tileHeight);
                        actor.X = tileX;
                        actor.Y = tileY;
                        mActorFollow = actor;
                        mHeroController.SetHero(actor);
                        newLayer.actors.Add(actor);
                        break;
                    }
                    case mTankTile:
                    {
                        break;
                        ActorTank actor = new ActorTank(tileWidth, tileHeight);
                        actor.X = tileX;
                        actor.Y = tileY;
                        newLayer.actors.Add(actor);
                        break;
                    }
                    default:

                        Sprite newSprite = new Sprite(tileWidth, tileHeight);
                        newSprite.SetAtlas(mTextureAtlas);
                        newSprite.SetFrame(mTextureAtlas.GetFrame(tileType-1).id);
                        newSprite.X = tileX;
                        newSprite.Y = tileY;
                        newSprite.Z = tileZ;
                        newLayer.tiles.Add(newSprite);
                        if (collidable)
                        {
                            PhysicsBody newBody = new PhysicsBody(game.Global.PhysicsEngine);
                            newBody.Id = "Tile : [" + row +","+col+"]";
                            newBody.SetDimensions(new pax.math.Rect(tileX, tileY, tileWidth, tileHeight));
                            game.Global.PhysicsEngine.RegisterBody(newBody);
                        }
                        break;
                    }
                }
                
            }
            mLayers[newLayer.name] = newLayer;
        }
    
    }

    #region IUpdatable implementation
    public void Update (int elapsedMs)
    {
        // We know that we only care to update the actors, 
        // which we have conveniently put in just 1 spot
        foreach(Actor actor in mLayers["spawns"].actors)
        {
            actor.AddForce("gravity", mGravity);
            actor.Update(elapsedMs);
        }

        mHeroController.Update(elapsedMs);

        mCameraFollow.x = mActorFollow.X;
        mCameraFollow.y = mActorFollow.Y;

        Camera.main.transform.position = mCameraFollow;

    }

    public void Shutdown ()
    {
        //throw new System.NotImplementedException ();
    }
    #endregion
    
}

}
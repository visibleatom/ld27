using System.Collections;
using System.Collections.Generic;

namespace game {
public class ActorTank : Actor 
{
    public ActorTank(float width, float height) : base(width, height)
    {

        ArrayList animFrames = new ArrayList();
        animFrames.Add("64Asleep0");
        mAnim.AddAnimation("sleep", animFrames );
        
        animFrames = new ArrayList();
        animFrames.Add("64patrol0");
        animFrames.Add("64patrol1");
        animFrames.Add("64patrol2");
        animFrames.Add("64patrol3");
        animFrames.Add("64patrol4");
        animFrames.Add("64patrol5");
        animFrames.Add("64patrol6");
        animFrames.Add("64patrol7");
        animFrames.Add("64patrol8");
        animFrames.Add("64patrol9");
        animFrames.Add("64patrol10");
        animFrames.Add("64patrol11");
        animFrames.Add("64patrol12");
        animFrames.Add("64patrol13");
        animFrames.Add("64patrol14");
        animFrames.Add("64patrol15");
        animFrames.Add("64patrol16");
        animFrames.Add("64patrol17");
        animFrames.Add("64patrol18");
        animFrames.Add("64patrol19");
        animFrames.Add("64patrol20");
        animFrames.Add("64patrol21");
        animFrames.Add("64patrol22");
        animFrames.Add("64patrol23");
        animFrames.Add("64patrol24");
        animFrames.Add("64patrol25");
        animFrames.Add("64patrol26");
        animFrames.Add("64patrol27");
        animFrames.Add("64patrol28");
        animFrames.Add("64patrol29");
        animFrames.Add("64patrol30");
        mAnim.AddAnimation("patrol", animFrames ); 

        mSprite.SetAtlas(game.Global.EnemyAtlas);

        mAnim.SetDefaultAnimation("sleep");
        mAnim.StartAnimation("patrol");
    }

}

}
using pax.core;
using pax.input;
using System.Collections.Generic;
using System.Collections;

namespace game {
public class UserInterface : IUpdatable
{

    ActorTank mTank;
    ActorRedglove mHero;

    public UserInterface()
    {
        UnityEngine.Debug.Log("UserInterface ctor");
        mHero = new ActorRedglove(64f,64f);
        mTank = new ActorTank(64f, 64f);
        mTank.X += 5;
        game.Global.Input.AddSynonymKeyCode(InputCodes.DIRECTION_RIGHT, UnityEngine.KeyCode.RightArrow);
        game.Global.Input.AddSynonymKeyCode(InputCodes.DIRECTION_RIGHT, UnityEngine.KeyCode.D);
        game.Global.Input.AddSynonymKeyCode(InputCodes.DIRECTION_LEFT, UnityEngine.KeyCode.LeftArrow);
        game.Global.Input.AddSynonymKeyCode(InputCodes.DIRECTION_LEFT, UnityEngine.KeyCode.A);
        game.Global.Input.AddSynonymKeyCode(InputCodes.RUN_MODIFIER, UnityEngine.KeyCode.LeftShift);
        game.Global.Input.AddSynonymKeyCode(InputCodes.RUN_MODIFIER, UnityEngine.KeyCode.RightShift);
    }

    public void SetLogProvider(ILogProvider provider)
    {

    }

    #region IUpdatable implementation
    public void Update (int elapsedMs)
    {
        mHero.Update(elapsedMs);
        mTank.Update(elapsedMs);


        bool walkRight = false;
        bool walkLeft = false;
        bool justStoppedWalking = false;
        InputInfo inputInfo;
        inputInfo = game.Global.Input.GetInputInfo(InputCodes.DIRECTION_RIGHT);
        if (inputInfo.Active)
        {
            walkRight = true;
        }
        else if (inputInfo.JustReleased)
        {
            walkRight = false;
            justStoppedWalking = true;
        }
        inputInfo = game.Global.Input.GetInputInfo(InputCodes.DIRECTION_LEFT);
        if (inputInfo.Active)
        {
            walkLeft = true;
        }
        else if (inputInfo.JustReleased)
        {
            walkLeft = false;
            justStoppedWalking = true;
        }        

        bool running = game.Global.Input.GetInputInfo(InputCodes.RUN_MODIFIER).Active;

        if (walkRight)
        {
            mHero.GoRight(running);
        }
        else if (walkLeft)
        {
            mHero.GoLeft(running);
        }
        else if (justStoppedWalking)
        {
            mHero.StopWalking();
        }

    }

    public void Shutdown ()
    {
        //throw new System.NotImplementedException ();
    }
    #endregion
    
}
}

using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour
{
    public Transform SpritePrefab;
    public SpriteAtlasContainer HeroAtlas;
    public SpriteAtlasContainer EnemyAtlas;
    public LevelContainer[] LevelAtlas;

    private game.MainGame mMain;
    private bool mDestroying = false;
    // Use this for initialization
    void Start () 
    {
        mMain = new game.MainGame(SpritePrefab, HeroAtlas, EnemyAtlas, LevelAtlas);

        mMain.Start();
    }
    
    // Update is called once per frame
    void Update () 
    {
        // This is just in case of a rare case, while debugging, where this can be hit BEFORE Start(), above.
        if (mMain == null) return;
        #region Camera resize from http://gamasutra.com/blogs/JoshSutphin/20130519/192539/Making_2D_Games_With_Unity.php
        // set the camera to the correct orthographic size
        // (so scene pixels are 1:1)
        float s_baseOrthographicSize = Screen.height / 4.0f;
        Camera.main.orthographicSize = s_baseOrthographicSize;        
        #endregion

        mMain.Update();
        if (!mMain.GameAlive && !mDestroying)
        {
            mDestroying = true;
            Destroy (this);
        }
    }

    void Destroy()
    {
        mMain.ShutdownSystems();
    }
}


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace pax { namespace core {

public class StateMachine : IStateMachine
{

    private struct Transition
    {
        public int id;
        public State origin;
        public State destination;
    };


    private Dictionary<string, State> mStates;
    private Dictionary<string, Transition> mTransitions;

    private State mCurrentState;

    private Transition mCurrentTransition;

    public StateMachine()
    {
        mStates = new Dictionary<string, State>();
        mCurrentTransition.id = 0;
        mCurrentTransition.origin = null;
        mCurrentTransition.destination = null;

        mCurrentState = null;
    }

    // This returns the human-readable state id
    // The same state can be stored multiple times with different ids, if so desired.
    public string AddState(string id, State newState)
    {
        if (!mStates.ContainsKey(id))
        {
            mStates.Add(id, newState);
            return id;
        }
        return null;
    }



    // This will exit the current state (if it's not the desiredState already) and 
    // then load desiredState. There is no transition check. We need to cancel any transitions currently
    // taking place? Or should we let the transitions finish, THEN change state?
    public void ChangeToState(string desiredState)
    {

    }


    // We'll try to transition out of the current state through the transition whose
    // id corresponds to transitionId. If the currentState does not have a valid transition
    // that matches, this will return false. If the transition is able to be started, then this
    // will return true. Please note that this does NOT mean that the transition has finished by the
    // time that this function returns. The transition can take many 
    public bool PerformStateTransition(string transitionId)
    {
        return false;
    }

    // AddTransition has the following requirements:
    // * originState and goalState must both exist in the list of states handled by this state machine
    // * there must not be another 
    public void AddTransition(string originState, string goalState, string transitionId)
    {

    }

    // This should only be called from within the states themselves.
    public void StateEnterFinished(State enterState, bool success)
    {
        
    }

    public void StateExitFinished(State exitState, bool success)
    {

    }
}

}}
using System.Collections;
namespace pax { namespace core {

public class State 
{
    private enum InternalState
    {
        INACTIVE,
        ENTERING,
        ACTIVE,
        EXITING,
    };

    private InternalState mInternalState;
    private StateMachine mStateMachine;

    public State(StateMachine fsm)
    {
        mStateMachine = fsm;
        mInternalState = InternalState.INACTIVE;
    }


    // This can be totally overridden, but in the meantime, we can
    // at least provide a fairly standard implementation
    public void StateEnter()
    {
        if (mInternalState == InternalState.INACTIVE)
        {
            InternalStateEnter();
        }
        else
        {
            // We're currently in a transition state or else already active ... Meaning
            // and please bear with me, because this really means that we need
            // to rethink whatever's going on here, since we have the FSM that 
            // apparently is not respecting its own rules.
            // We need to first EXIT this state, and only then can we try to re-enter
            // it. The FSM should already know this.
            mStateMachine.StateEnterFinished(this, false);
        }
    }

    public void StateExit()
    {
        if (mInternalState == InternalState.ACTIVE)
        {
            InternalStateExit();
        }
        else
        {
            // We're currently in a transition state or else already active ... Meaning
            // and please bear with me, because this really means that we need
            // to rethink whatever's going on here, since we have the FSM that 
            // apparently is not respecting its own rules.
            // We need to first EXIT this state, and only then can we try to re-enter
            // it. The FSM should already know this.
            mStateMachine.StateExitFinished(this, false);
        }
    }

    protected virtual void InternalStateExit()
    {
        mInternalState = InternalState.INACTIVE;
        mStateMachine.StateExitFinished(this, true);
    }

    protected virtual void InternalStateEnter()
    {
        mInternalState = InternalState.ACTIVE;
        mStateMachine.StateEnterFinished(this, true);    
    }
}

}}

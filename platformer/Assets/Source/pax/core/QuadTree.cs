using UnityEngine;
using System.Collections;

namespace pax { namespace core {

// Quadtrees are fairly standard, but I'm explicitly borrowing from the following page 
// http://gamedev.tutsplus.com/tutorials/implementation/quick-tip-use-quadtrees-to-detect-likely-collisions-in-2d-space/

    // MASSIVE TODO - why the hell do people use recursion so much? Use traversal nodes, people! 
public class QuadTree
{
    const int MAX_OBJECTS = 10;
    const int MAX_LEVELS = 10; //< for optimization purposes only, which is why we're keeping it large.

    private int mLevel;
    private ArrayList mObjects;
    private QuadTree[] mNodes;
    private Rect mBounds;
    private QuadTree mParent;

    public QuadTree(QuadTree parent, int level, Rect bounds)
    {
        mParent = parent;
        mLevel = level;
        mBounds = bounds;
        mObjects = new ArrayList();
        mNodes = new QuadTree[4];
    }

    public void Clear() 
    {
       mObjects.Clear();
     
       for (int i = 0; i < mNodes.Length; i++) 
       {
         if (mNodes[i] != null) 
         {
           mNodes[i].Clear();
           mNodes[i] = null;
         }
       }
     }

    private void Split() 
    {
       int subWidth = (int)(mBounds.width / 2);
       int subHeight = (int)(mBounds.height / 2);
       int x = (int)mBounds.x;
       int y = (int)mBounds.y;
     
       mNodes[0] = new QuadTree(this, mLevel+1, new Rect(x + subWidth, y, subWidth, subHeight));
       mNodes[1] = new QuadTree(this, mLevel+1, new Rect(x, y, subWidth, subHeight));
       mNodes[2] = new QuadTree(this, mLevel+1, new Rect(x, y + subHeight, subWidth, subHeight));
       mNodes[3] = new QuadTree(this, mLevel+1, new Rect(x + subWidth, y + subHeight, subWidth, subHeight));
     }     

    /*
     * Determine which node the object belongs to. -1 means
     * object cannot completely fit within a child node and is part
     * of the parent node
     Indexes are:
     1 0
     2 3
     with -1 being that it CANNOT fit in just one of those quadrants
     */
    private int GetIndex(IQuadTreeNode obj) 
    {

        int index = -1;
        double verticalMidpoint = mBounds.x + (mBounds.width / 2f);
        double horizontalMidpoint = mBounds.y + (mBounds.height / 2f);

        // Object can completely fit within the top quadrants
        bool topQuadrant = (obj.GetY() < horizontalMidpoint && obj.GetY() + obj.GetHeight() < horizontalMidpoint);
        // Object can completely fit within the bottom quadrants
        bool bottomQuadrant = (obj.GetY() > horizontalMidpoint);

        // Object can completely fit within the left quadrants
        if (obj.GetX() < verticalMidpoint && obj.GetX() + obj.GetWidth() < verticalMidpoint)
        {
            if (topQuadrant)
            {
                index = 1;
            }
            else if (bottomQuadrant)
            {
                index = 2;
            }
        }
        // Object can completely fit within the right quadrants
        else if (obj.GetX() > verticalMidpoint)
        {
            if (topQuadrant)
            {
                index = 0;
            }
            else if (bottomQuadrant)
            {
                index = 3;
            }
        }

        return index;
    }     

    /*
    * Insert the object into the quadtree. If the node
    * exceeds the capacity, it will split and add all
    * mObjects to their corresponding mNodes.
    */
    public void Insert(IQuadTreeNode obj)
    {
        if (mNodes[0] != null)
        {
            int index = GetIndex(obj);

            if (index != -1)
            {
                mNodes[index].Insert(obj);

                return;
            }
        }

        // This object couldn't find a home under any of my
        // children. So it will live here, for now.
        mObjects.Add(obj);
        obj.SetQuadTree(this);

        if (mObjects.Count > MAX_OBJECTS && mLevel < MAX_LEVELS)
        {
            // If I haven't been subdivided yet, then subdivide.
            if (mNodes[0] == null)
            { 
                Split(); 
            }

            int i = 0;
            IQuadTreeNode insertObj;

            while (i < mObjects.Count)
            {
                insertObj = (IQuadTreeNode)mObjects[i];
                int index = GetIndex(insertObj);
                if (index != -1)
                {
                    mObjects.Remove(insertObj);
                    insertObj.SetQuadTree(null);
                    mNodes[index].Insert(insertObj);
                }
                else
                {
                    i++;
                }
            }
        }

    }

    /*
    * Return all objects that could collide with the given object
    */
    public ArrayList Retrieve(ArrayList returnObjects, IQuadTreeNode obj)
    {
        int index = GetIndex(obj);
        if (index != -1 && mNodes[0] != null)
        {
            mNodes[index].Retrieve(returnObjects, obj);
        }

        returnObjects.AddRange(mObjects);

        return returnObjects;
    }    

    public void UpdatePosition(IQuadTreeNode obj)
    {
        int index = GetIndex(obj);
        if (index != -1)
        {
            // This means that the object can fit in one of
            // our subquadrants. We shouldn't own it anymore.
            mObjects.Remove(obj);
            obj.SetQuadTree(null);
            // Insert will create mNodes (split this quadrant) if needed.
            Insert(obj);
            return;
        }

        // Well, didn't work for us, so either the object fits within us perfectly
        // (in which case we won't try anything) 
        if (mBounds.x >= obj.GetX() && mBounds.y >= obj.GetY() && mBounds.width <= obj.GetWidth() && mBounds.height <= obj.GetHeight())
        {
            // I said we weren't going to move this guy.
            return;
        }
        // OR!
        // up the parent until we reach the case where either parent is null (meaning
        // we reached the root) or else index != -1
        // We will remove the object NOW, and restore it when we finally find the root or a good index
        QuadTree traverse = this;
        while(traverse.mParent != null)
        {
            index = traverse.GetIndex(obj);
            if (index != -1)
            {
                traverse.Insert(obj);
                return;
            }
            traverse = traverse.mParent;
        }

        // No choice. We have to insert it here
        traverse.Insert(obj);
    }

}

}}
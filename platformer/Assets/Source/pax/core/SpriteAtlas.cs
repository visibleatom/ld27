using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Xml;
using System;

namespace pax { namespace core {

public class SpriteAtlas
{
    public struct AtlasFrame
    {
        public string id;
        public int index;
        public float x;
        public float y;
        public float width;
        public float height;

    };
    public Texture mTexture;

    public System.Xml.XmlDocument mData;

    private Dictionary<string, AtlasFrame> mFrames;
    private Dictionary<int, string> mIndexMap;

    public SpriteAtlas(Texture texture, System.Xml.XmlDocument xmlData)
    {
        //Debug.Log("SpriteAtlas constructor");
        mFrames = new Dictionary<string, AtlasFrame>();
        mIndexMap = new Dictionary<int, string>();

        SetAtlasData(texture, xmlData);
    }

    public AtlasFrame GetFrame(int index)
    {
        return mFrames[mIndexMap[index]];
    }

    public AtlasFrame GetFrame(string id)
    {
        return mFrames[id];
    }

    public void SetAtlasData(Texture texture, System.Xml.XmlDocument xmlData)
    {

        mTexture = texture;
        mData = xmlData;
        ProcessGenericXml();
    }

    private void ProcessGenericXml()
    {
        XmlNode textureAtlas = mData.SelectSingleNode("/TextureAtlas");
        XmlNode traverseNode = textureAtlas.FirstChild;
        int index = 0;
        while (traverseNode != null)
        {

            AtlasFrame newFrame = new AtlasFrame();
            newFrame.id = traverseNode.Attributes["n"].Value;
            newFrame.index = index++;
            newFrame.x = float.Parse(traverseNode.Attributes["x"].Value);
            newFrame.y = float.Parse(traverseNode.Attributes["y"].Value);
            newFrame.width = float.Parse(traverseNode.Attributes["w"].Value);
            newFrame.height = float.Parse(traverseNode.Attributes["h"].Value);
            
            mFrames.Add(newFrame.id, newFrame);
            mIndexMap.Add(newFrame.index, newFrame.id);

            traverseNode = traverseNode.NextSibling;
        }
    }

    private void ProcessCocos2DPlist()
    {

        XmlNode keyDict = mData.SelectSingleNode("/plist//dict");
        XmlNode traverseNode = keyDict.FirstChild;
        // We expect the node to be showing "frames" now.
        if (traverseNode.LocalName != "key" || traverseNode.InnerText != "frames")
        {
            // ERROR!
            return;
        }
        traverseNode = traverseNode.NextSibling;
        if (traverseNode.LocalName != "dict") //< look, we gotta assume that these are the frames we're looking at, here
        {
            // ERROR! 
            return;
        }

        XmlNode metadata = traverseNode.NextSibling; //< We don't need this, given we already have the texture

        // Grab all the frames in here and be happy, I guess.
        traverseNode = traverseNode.FirstChild;
        XmlNode nextNode;
        while (traverseNode != null && traverseNode.LocalName == "key")
        {
            AtlasFrame newFrame = new AtlasFrame();
            newFrame.id = traverseNode.InnerText;

            traverseNode = traverseNode.NextSibling; //< dict
            nextNode = traverseNode.NextSibling; //< the next 'key' node

            // The children in the dict are, as far as we know, as follows:
            // key (frame), string ({{x,y},{w,h}}), offset ({x,y}), string, key, false, key, string, key, string.
            traverseNode = traverseNode.FirstChild; //< key (frame)
            traverseNode = traverseNode.NextSibling; //< string (coordinates)
            string coords = traverseNode.InnerText;
            char[] separators = new char[] {'{','}',','};
            string[] sides = coords.Split(separators);
            newFrame.x = float.Parse(sides[2]);
            newFrame.y = float.Parse(sides[3]);
            newFrame.width = float.Parse(sides[6]);
            newFrame.height = float.Parse(sides[7]);

            mFrames.Add(newFrame.id, newFrame);

            traverseNode = nextNode; //< the next 'key' node
        }
    }
}

}}
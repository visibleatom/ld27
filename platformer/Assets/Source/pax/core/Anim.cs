using System.Collections;
using System.Collections.Generic;

namespace pax { namespace core {

public class Anim
{
    public struct AnimInfo
    {
        public string id;
        public ArrayList frames;
        public int fps;
        public int repeatCounter;
    }

    Dictionary<string, AnimInfo> mAnimations;

    AnimInfo mCurrentAnim;
    string mDefaultAnim;
    int mCurrentFrame;
    int mFps;
    //int mMsPerFrame;
    int mTimeSinceLastFrame;
    public Anim()
    {
        mAnimations = new Dictionary<string, AnimInfo>();
        mCurrentFrame = 0;
        mCurrentAnim = new AnimInfo();
        mDefaultAnim = "";
        SetFps(30);
    }

    public void SetDefaultAnimation(string id)
    {
        mDefaultAnim = id;
    }

    public void SetFps(int fps)
    {
        mFps = fps;
        //mMsPerFrame = 1000 / mFps;
    }
    public void CancelAnimation()
    {
        mCurrentFrame = 0;
        mCurrentAnim = mAnimations[mDefaultAnim]; // Even if it's just 1 frame, we gotta have something.
    }

    public void StartAnimation(string animId)
    {
        StartAnimation (animId, -1, -1);
    }
    public void StartAnimation(string animId, int repeatCounter, int fps)
    {
        if (mCurrentAnim.id != animId)
        {
            mCurrentFrame = 0;
            mCurrentAnim = mAnimations[animId];
            mCurrentAnim.repeatCounter = repeatCounter;
            mCurrentAnim.fps = fps;
        }
    }

    public string GetFrameId()
    {
        return (string)mCurrentAnim.frames[mCurrentFrame];
    }

    public void AddAnimation(string id, ArrayList frames)
    {
        AnimInfo info = new AnimInfo();
        info.id = id;
        info.frames = frames;
        info.fps = mFps;
        info.repeatCounter = 0;
        mAnimations.Add(id, info);
    }


    public void Update(int elapsedMs)
    {
        if (mCurrentAnim.repeatCounter != 0)
        {
            mTimeSinceLastFrame += elapsedMs;
            int advanceFrame = 0;
            int msPerFrame = 1000 / ((mCurrentAnim.fps > 0) ? mCurrentAnim.fps : mFps);

            if (mTimeSinceLastFrame > msPerFrame)
            {
                int remainder = mTimeSinceLastFrame % msPerFrame;
                advanceFrame = (mTimeSinceLastFrame / msPerFrame);
                mTimeSinceLastFrame = remainder;
            }

            int maxFrames = mCurrentAnim.frames.Count;
            if (advanceFrame + mCurrentFrame >= maxFrames)
            {
                mCurrentAnim.repeatCounter--;
            }

            if (mCurrentAnim.repeatCounter != 0)
            {
                mCurrentFrame = (advanceFrame + mCurrentFrame) % maxFrames;
            }
            else
            {
                // we're at repeatCounter == 0. This means we're DONE.
                mCurrentFrame = maxFrames - 1;
            }
        }
    }

}

}}
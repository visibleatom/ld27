namespace pax { namespace core {
public interface IUpdatable
{
    void Update(int elapsedMs);

    void Shutdown();
}

}}

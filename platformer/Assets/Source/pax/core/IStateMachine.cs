namespace pax { namespace core {

public interface IStateMachine  
{
    string AddState(string id, State newState);
    void ChangeToState(string desiredState);
    bool PerformStateTransition(string transitionId);
    void AddTransition(string originState, string goalState, string transitionId);
    void StateEnterFinished(State enterState, bool success);
    void StateExitFinished(State exitState, bool success) ;
}

}}
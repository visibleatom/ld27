using UnityEngine;
using System.Collections;


namespace pax { namespace core {
[System.Serializable]
public class Sprite 
{
    #region unity-only
    public static void SetUnityPrefab(UnityEngine.Transform quadPrefab) { QUAD_PREFAB = quadPrefab; }
    private static UnityEngine.Transform QUAD_PREFAB;
    #endregion

    public float X { get { return mX; }  set {
                    if (float.IsInfinity(value))
                    {
                        Debug.Log("PLEASE CHECK THIS");
                    }

                    mX = mPositionVector.x = value; mQuad.transform.position = mPositionVector;}}
    public float Y { get { return mY; }  set { mY = mPositionVector.y = value; mQuad.transform.position = mPositionVector;}}
    public float Z { get { return mZ; }  set { mZ = mPositionVector.z = value; mQuad.transform.position = mPositionVector;}}

    public float ScaleX { get { return mLocalScale.x; } set {Debug.Log("Setting ScaleX = " + value);mLocalScale.x = value; mQuad.transform.localScale = mLocalScale;}}
    public float ScaleY { get { return mLocalScale.y; } set {mLocalScale.y = value; mQuad.transform.localScale = mLocalScale;}}

    private float mX;
    private float mY;
    private float mZ;
    private Vector3 mPositionVector; //< I do this to avoid a 'new' call in SetPosition
    private UnityEngine.Transform mQuad;

    private string mAtlasFrame;
    private SpriteAtlas mAtlas;
    Vector3 mLocalScale;

    private float mTargetHeight = 64f;
    private float mTargetWidth = 64f;
    private bool mFlipHorizontal;
    private bool mFlipVertical;

    // constructor
    public Sprite(float targetWidth, float targetHeight)
    {
        mFlipHorizontal = false;
        mFlipVertical = false;
        mTargetWidth = targetWidth;
        mTargetHeight = targetHeight;
        mAtlasFrame = "";
        mAtlas = null;
        mQuad = (Transform)UnityEngine.Object.Instantiate(QUAD_PREFAB);
        mPositionVector = mQuad.transform.position;
        mLocalScale = mQuad.transform.localScale;
        mLocalScale.x = targetWidth;
        mLocalScale.y = targetHeight;

    }

    public void Flip(bool horizontal, bool vertical)
    {
        mFlipHorizontal = horizontal;
        mFlipVertical = vertical;
    }

    public void SetAtlas(SpriteAtlas atlas)
    {
        mAtlas = atlas;
        mQuad.renderer.material.mainTexture = atlas.mTexture;
    }

    public void SetFrame(string id)
    {
        mAtlasFrame = id;
        // Here is the logic that I will eventually have spread out between
        // the Sprite and the SpriteAtlas (mostly in the atlas)
        // http://gamasutra.com/blogs/JoshSutphin/20130519/192539/Making_2D_Games_With_Unity.php
        // Vector2[] uvs       = new Vector2[m_mesh.uv.Length];
        // Texture texture     = m_meshRenderer.sharedMaterial.mainTexture;

        Mesh mesh = mQuad.GetComponent<MeshFilter>().mesh;
        Vector2[] uvs = new Vector2[mesh.uv.Length];
        Texture texture = mAtlas.mTexture;

        // Vector2 pixelMin    = new Vector2(
        //     (float)m_currentStrand.frames[m_animFrame].x /
        //         (float)texture.width,
        //     1.0f - ((float)m_currentStrand.frames[m_animFrame].y / 
        //         (float)texture.height));

        //Debug.Log("keys : " + atlas.GetKey(keys));
        SpriteAtlas.AtlasFrame frame = mAtlas.GetFrame(id);
        float animFrameX = frame.x;
        float animFrameY = frame.y;
        float animFrameWidth = frame.width;
        float animFrameHeight = frame.height;

        // Let's grab the first frame just as a test.
        Vector2 pixelMin = new Vector2(
                animFrameX / (float) texture.width,
                1f - (animFrameY / (float) texture.height)
            );

        // Vector2 pixelDims   = new Vector2(
        //     (float)m_currentStrand.frames[m_animFrame].width / 
        //         (float)texture.width,
        //     -((float)m_currentStrand.frames[m_animFrame].height / 
        //         (float)texture.height));

        // We know the frame is 32x32
        Vector2 pixelDims = new Vector2( animFrameWidth / (float)texture.width, 
            -animFrameHeight / (float)texture.height);

        // // main mesh
        {
            Vector2 min = pixelMin;// + m_textureOffset;
            uvs[0] = min + new Vector2(pixelDims.x * 1.0f, pixelDims.y * 0.0f);
            uvs[1] = min + new Vector2(pixelDims.x * 1.0f, pixelDims.y * 1.0f);
            uvs[3] = min + new Vector2(pixelDims.x * 0.0f, pixelDims.y * 0.0f);
            uvs[2] = min + new Vector2(pixelDims.x * 0.0f, pixelDims.y * 1.0f);

            //Debug.Log("pixelDims : " + pixelDims.ToString());
            //Debug.Log("uvs : " + uvs[0].ToString() + ", " + uvs[1].ToString()+ ", " + uvs[2].ToString()+ ", " + uvs[3].ToString());
            mLocalScale.x = animFrameWidth * (mFlipHorizontal?-1f:1f);
            mLocalScale.y = animFrameHeight * (mFlipVertical?-1f:1f);;
            mQuad.localScale = mLocalScale;
            //Debug.Log("localScale : "+ mQuad.localScale.ToString());
            //Debug.Log("mesh.uv : " + mesh.uv[0].ToString() + ":::" + mesh.uv[1].ToString());
            mesh.uv = uvs;
        }        
    }

    // Sets both x and y together
    public void SetPosition(float x, float y)
    {
        mX = x;
        mY = y;
        // transform.position is a read-only attribute, but it CAN be
        // set. So I can just modify my stored position vector (avoiding a
        // 'new' at this point, hurray) and assign it as the current position
        // for the Unity quad.
        mPositionVector.x = mX;
        mPositionVector.y = mY;
        mQuad.transform.position = mPositionVector;
    }



}

}}
using UnityEngine;
using System.Collections;
namespace pax { namespace core {
public interface IQuadTreeNode
{
    void SetQuadTree(QuadTree t);
    float GetWidth();
    float GetHeight();
    float GetX();
    float GetY();
}
}}
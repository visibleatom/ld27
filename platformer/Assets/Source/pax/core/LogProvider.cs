using UnityEngine;
namespace pax { namespace core {
public class LogProvider : ILogProvider 
{
	
	private bool mLogEnabled;
	
	public LogProvider ()
	{
		mLogEnabled = false;
	}

	
	
	#region ILogProvider implementation
	public void Log (object message)
	{
		if (mLogEnabled)
		{
			Debug.Log(message);
		}
	}

	public void Log (object message, Object context)
	{
		if (mLogEnabled)
		{
			Debug.Log (message, context);
		}
	}
	
	public void SetLogEnabled (bool enabled)
	{
		mLogEnabled = enabled;
	}
	#endregion
}
}}
﻿namespace pax.math
{

    public class Rect
    {
        public float MinX { get { return (Width < 0 ? X - Width : X); } }
        public float MaxX { get { return (Width < 0 ? X : X + Width); } }
        public float MinY { get { return (Height < 0 ? Y - Height : Y); } }
        public float MaxY { get { return (Height < 0 ? Y : Y + Height); } }
        public float X;
        public float Y;
        public float Width;
        public float Height;

        public Rect(float _x, float _y, float _width, float _height)
        {

        }
    }
}
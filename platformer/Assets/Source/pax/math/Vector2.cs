using pax.util;


namespace pax.math
{

    // This is a reusable, poolable Vector2 class. This contains
    // x,y, and all relevant functionality normally present for vector
    // math.
    // The reusability aspect is critical. Through a number of platforms,
    // it has seemed very clear that memory allocation is a terrible hassle and
    // allocating a good-sized amount of objects at the start of a mode (if they will
    // be mostly used) is much better than allocating/de-allocating them at runtime. 
    // This is SPECIALLY brutal with particle systems, but even something as simple 
    // as creating (and deleting) a new vector to calculate the difference between two points 
    // is terribly inefficient.
    // This can still be used without its recycling capabilities.
    public class Vector2 : IRecyclable<Vector2>
    {
        public Vector2 GetNewRecyclable() { return ZERO(); }
        public static Vector2 ZERO() { return new Vector2(0,0); }
        public static Vector2 ONE() { return new Vector2(1.0f,1.0f); }

        private static int INVALID_ID = pax.util.ObjectPoolManager.INVALID_ID;

        public float X;
        public float Y;
        
        // This is what allows the reusable pool to recognize this.
        private int mRecycleId;

        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
            mRecycleId = INVALID_ID;
        }
        
        public Vector2()
        {
            mRecycleId = INVALID_ID;
        }
        
        

        bool Equals(Vector2 b)
        {
            return (b.X == X && b.Y == Y);
        }


        void Add(Vector2 b)
        {
            X += b.X;
            Y += b.Y;
        }
        
        void Subtract(Vector2 b) 
        {
            X -= b.X;
            Y -= b.Y;
            
        }
       
        public void SetXY(float x, float y)
        {
            X = x;
            Y = y;
        }
        
        void Multiply(float scalar)
        {
            X *= scalar;
            Y *= scalar;
        }

        void Divide(float scalar)
        {
            X /= scalar;
            Y /= scalar;
        }

        public float Magnitude()
        {
            return (float)Math.sqrt(MagnitudeSquared());
        }

        public float MagnitudeSquared()
        {
            return (X * X + Y * Y);
        }
        
        void Normalize()
        {
            float magnitude = Magnitude();
            X /= magnitude;
            Y /= magnitude;
        }
        
        public bool IsZeroLength()
        {
            return MagnitudeSquared() == 0;
        }

        // It is COMPLETELY unsafe to use this object after calling Recycle() 
        // on it. COMPLETELY unsafe.  For all intents and purposes, imagine 
        // someone else took control of this as soon as we called Recycle on it
        // and as such, the values might not conform to expectations.
        public void Recycle()
        {
            if (mRecycleId != INVALID_ID)
            {       
                SetXY(0.0f, 0.0f);
                int recycleId = mRecycleId;
                mRecycleId = INVALID_ID;         
                ObjectPoolManager a = ObjectPoolManager.Instance;

                a.Get<Vector2>().ReleaseData(recycleId);
            }
        }
        
        public static float Distance(Vector2 a, Vector2 b)
        {
            return (float)Math.sqrt(DistanceSquared(a,b));
        }
        
        public static float DistanceSquared(Vector2 a, Vector2 b)
        {
            float dX = a.X - b.X;
            float dY = a.Y - b.Y;
            
            return (dX * dX + dY * dY);
        }
        
        public static Vector2 GetRecyclable()
        {
            ObjectPool<Vector2> recycler = ObjectPoolManager.Instance.Get<Vector2>();
            int reservedId = recycler.ReserveDataId();
            Vector2 recyclable = recycler.GetReservedData(reservedId);
            recyclable.mRecycleId = reservedId;
            return recyclable;
        }
        // The reasoning behind making these NOT return a recyclable Vector2 is
        // that it is assumed 
        public static Vector2 Subtract(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }
        
        public static Vector2 Add(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }

    }
}
using UnityEngine;
using System.Collections;


public class LevelContainer : MonoBehaviour {

    public Texture mAtlasTexture;
    public TextAsset mAtlasData;
    public TextAsset mLevelJson;
    
    private System.Xml.XmlDocument mXmlData;

    // Use this for initialization
    void Start () 
    {
        //Debug.Log("SpriteAtlasContainer started");
        mXmlData = new System.Xml.XmlDocument();

        mXmlData.LoadXml(mAtlasData.text);
    }

    public System.Xml.XmlDocument GetAtlasDataXml()
    {
        if (mAtlasData != null && mXmlData == null)
        {
            mXmlData = new System.Xml.XmlDocument();
            mXmlData.LoadXml(mAtlasData.text);
        }

        return mXmlData;

    }

}

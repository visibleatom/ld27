using UnityEngine;
using System.Collections;

// I wish I could namespace this and still have it work with Unity. Alas...

public class SpriteAtlasContainer : MonoBehaviour 
{

    public Texture mTexture;
    public TextAsset mDataFile;

    private System.Xml.XmlDocument mXmlData;
    // Use this for initialization
    void Start () 
    {
        //Debug.Log("SpriteAtlasContainer started");
        mXmlData = new System.Xml.XmlDocument();

        mXmlData.LoadXml(mDataFile.text);
    }

    public System.Xml.XmlDocument GetXmlData()
    {
        if (mDataFile != null && mXmlData == null)
        {
            mXmlData = new System.Xml.XmlDocument();
            mXmlData.LoadXml(mDataFile.text);
        }

        return mXmlData;

    }

}

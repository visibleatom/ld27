using UnityEngine;
using System.Collections;
namespace pax { namespace input {
public interface IInputSystem 
{

    void AddSynonymKeyCode(string id, KeyCode rawCode);
    void AddSynonymPointerButton(string id, int buttonIndex);
    void AddSynonymTouchIndex(string id, int index);
    InputInfo GetInputInfo(string id);
}

}}
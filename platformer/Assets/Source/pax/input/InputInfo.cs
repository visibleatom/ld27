namespace pax
{
    namespace input
    {
        // I want to force InputInfo to be as read-only as possible.
        // However, since I know a little bit about optimizations, I also
        // want to have ways to reuse this object once it's constructed.
        // So I cannot just rely on the constructor as being the only way
        // to set the values in this class/struct.
        public class InputInfo
        {

            #region setters/getters
            public bool Handled 
            { 
                get { return this.mHandled; } 
                set { if (this.mWritable) this.mHandled = value; }
            }
            public bool Active 
            { 
                get { return this.mActive; } 
                set { if (this.mWritable) this.mActive = value; }
            }
            public bool JustPressed 
            {
                get { return this.mJustPressed; } 
                set { if (this.mWritable) this.mJustPressed = value; }
            }
            
            public bool JustReleased 
            { 
                get { return this.mJustReleased; } 
                set { if (this.mWritable) this.mJustReleased = value; }
            }
            
            public int TimeActiveMs 
            { 
                get { return this.mTimeActiveMs; } 
                set { if (this.mWritable) this.mTimeActiveMs = value; }
            }
            public int X 
            { 
                get { return this.mX; } 
                set { if (this.mWritable) this.mX = value; }
            }
            public int Y 
            { 
                get { return this.mY; } 
                set { if (this.mWritable) this.mY = value; }
            }
            #endregion

            private bool mHandled; //< if TRUE, then this has already been handled. Should be ignored, really.
            private int mX;  //< Context-sensitive value. For mouse/pointers, it's the current screen-coordinate position. For sticks, it's the magnitude of tilt.
            private int mY;  
            private bool mActive; //< A mouse can have this NOT be active, and yet have x,y. That just means they're not clicking it.
            private bool mJustPressed;
            private bool mJustReleased;
            private int mTimeActiveMs; //< time, in milliseconds, detailing how long this input has been active.

            private InputInfo mOriginInput;
            private bool mWritable;

            // Take in the original InputInfo which will provide us with a way to update
            // the read-only fields without exposing ourselves.
            public InputInfo(InputInfo originInput)
            {
                this.mOriginInput = originInput;
                this.mWritable = (this.mOriginInput == null);
                ResetData();
            }

            public void ResetData()
            {
                if (mWritable)
                {
                    this.mHandled = false;
                    this.mX = 0;
                    this.mY = 0;
                    this.mActive = false;
                    this.mJustPressed = false;
                    this.mJustReleased = false;
                    this.mTimeActiveMs = 0;
                }

            }

            // Handled can be set to true, but NEVER set to false manually by anyone with
            // an origin input. 
            // This is a hard rule: once handled, we cannot claim that it is suddenly
            // 'unhandled' elsewhere during this frame/usage stack.  We can still look at the 
            // input in case we wish to do something with it or simply react to it, regardless
            // of whether anyone else has handled it already.
            public void SetHandled()
            {
                this.mHandled = true;
                if (this.mOriginInput != null)
                {
                    this.mOriginInput.mHandled = true;
                }
            }

            // Reload the data from the origin input.
            public void ReloadData()
            {
                if (this.mOriginInput != null)
                {
                    this.mHandled = this.mOriginInput.mHandled;
                    this.mX = this.mOriginInput.mX;
                    this.mY = this.mOriginInput.mY;
                    this.mActive = this.mOriginInput.mActive;
                    this.mJustPressed = this.mOriginInput.mJustPressed;
                    this.mJustReleased = this.mOriginInput.mJustReleased;
                    this.mTimeActiveMs = this.mOriginInput.mTimeActiveMs;
                }
            }
            
            
        }

    }
}
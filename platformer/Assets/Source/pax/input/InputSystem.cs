
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using pax.core;

namespace pax { namespace input {

// This class reads the raw input and stores the game-specific codes that we
// want them to represent. 
public class InputSystem : IUpdatable, IInputSystem
{
    // Read-only attributes
    

    // These are modifiable inputs that will NOT be sent out into the world.
    private Dictionary<string, InputInfo> mRegisteredInputs;
    
    // The dispatched inputs are not modifiable and 
    private Dictionary<string, InputInfo> mDispatchedInputs; 

    // This is a dictionary of synonyms. Each id can have
    // multiple raw codes mapped to it.
    // ArrayList of KeyCode (int)    
    private Dictionary<string, ArrayList> mSynonymKeycodes;
    
    // This is a dictionary for pointer buttons. For instance,
    // left-click, right-click, middle-click, etc. Does not seem that
    // this would handle the mousewheel scroll. Tsk.
    private Dictionary<string, ArrayList> mSynonymPointerButtons;

    // TODO - touches (mobile, touch-monitors), analog axes (joysticks, sticks, gamepad shoulder triggers, etc)


    // This is currently unused, sadly.
    private Dictionary<KeyCode, bool> mKeycodeStates;
    
    private ILogProvider mLogProvider;
    
    public InputSystem ()
    {
    
        mLogProvider = null;
        
        mRegisteredInputs = new Dictionary<string, InputInfo>();
        mDispatchedInputs = new Dictionary<string, InputInfo>();
        
        mSynonymKeycodes = new Dictionary<string, ArrayList>();
        mSynonymPointerButtons = new Dictionary<string, ArrayList>();
        mKeycodeStates= new Dictionary<KeyCode, bool>();
    }

    #region IInputSystem implementation
    public void AddSynonymKeyCode(string id, KeyCode rawCode)
    {
        if (!mSynonymKeycodes.ContainsKey(id))
        {
            mSynonymKeycodes.Add(id, new ArrayList());
        }
        
        if (mSynonymKeycodes[id].Contains(rawCode))
        {
            //TODO - should I assert? throw an error?
            mLogProvider.Log("This id has already assigned this keycode! id=" + id + ", keycode=" + rawCode);
        }
        else
        {
            if (!mKeycodeStates.ContainsKey(rawCode))
            {
                mKeycodeStates.Add(rawCode, false);
            }

            mSynonymKeycodes[id].Add(rawCode);
            InternalAddRegisteredId(id);
        }
    }
    public void AddSynonymPointerButton(string id, int buttonIndex)
    {
        mLogProvider.Log("AddSynonymPointerButton id=" + id + ", buttonIndex=" + buttonIndex);
        if (!mSynonymPointerButtons.ContainsKey(id))
        {
            mSynonymPointerButtons.Add(id, new ArrayList());
        }

        if (mSynonymPointerButtons[id].Contains(buttonIndex))
        {
            //TODO - should I assert? throw an error?
            mLogProvider.Log("This id has already assigned this pointer button index! id=" + id + ", buttonIndex=" + buttonIndex);
        }
        else
        {
            mSynonymPointerButtons[id].Add(buttonIndex);

            InternalAddRegisteredId(id);
        }

    }
    public void AddSynonymTouchIndex(string id, int index)
    {
        // TODO
        mLogProvider.Log("AddSynonymTouchIndex not implemented yet");
    }

    public InputInfo GetInputInfo(string id)
    {
        if (!mDispatchedInputs.ContainsKey(id))
        {
            mLogProvider.Log("GetInputInfo is being called before the first Update() call! This is bad");
            // EARLY RETURN
            return null;
        }
        
        return mDispatchedInputs[id];
    }
    #endregion
    
    #region IUpdatable implementation
    // Update is called once per frame
    public void Update (int elapsedMs)
    {
        
        // For every registered input, check whether we have any of the synonyms 
        // active
        ArrayList synonyms = null;
        InputInfo info = null;
        string id;


        // Make sure that we have consistent results for 
        // multiple calls to GetKey(keycode) by calling it only once, and caching it.

        // I don't want to store the keys in a list and iterate through them,
        // but I cannot think of any other way at the moment.
        // So for now, we're just not storing this, and always asking Input.GetKey()
        /*
        foreach(KeyCode key in mKeycodeStates.Keys)
        {
            mKeycodeStates[key] = Input.GetKey(key);
        }
        */

        // Now go through each registered input and populate the current frame information
        foreach(KeyValuePair<string, InputInfo> pair in mRegisteredInputs)
        {
            id = pair.Key;
            info = pair.Value;
            
            // If we stick with the design, this will never need to be reset, as it will never be 
            // set to true in mSynonymKeycodes, but better safe than sorry.
            info.Handled = false;

            // Are any of the synonym keycodes for this held down?
            bool wasActive = info.Active;
            info.Active = false;
            info.JustPressed = false;
            info.JustReleased = false;

            if (mSynonymKeycodes.ContainsKey(id))
            {
                synonyms = mSynonymKeycodes[id];
                foreach(KeyCode syn in synonyms)
                {

                    // TODO -  Replace with cached value, when possible
                    if (Input.GetKey(syn))
                    {
                        info.Active = true;
                    }
                }
            }

            if (mSynonymPointerButtons.ContainsKey(id))
            {
                synonyms = mSynonymPointerButtons[id];
                foreach(int syn in synonyms)
                {

                    // TODO -  Replace with cached value, when possible
                    if (Input.GetMouseButton(syn))
                    {
                        info.Active = true;
                    }
                }
            }

            if (wasActive != info.Active)
            {
                // Alright, there was a change. Either we have just pressed it, or JUST released it.
                // Notice that we are avoiding the use of Unity's GetKeyDown and GetKeyUp. 
                // This is to more easily handle the following case:
                // Spacebar + E are both synonyms for "jump". Press E, hold. Press spacebar, hold. 
                // Release E. Did we release "jump", or not? This is taking the side of "still pressing jump".
                // Otherwise, we'd need to keep track of which one of the raw codes associated with a 
                // particular id triggered the initial active state.

                if (info.Active)
                {
                    info.JustPressed = true;
                    mLogProvider.Log("InputSystem::Update [" + id + "] JustPressed");
                }
                else
                {
                    info.JustReleased = true;
                    mLogProvider.Log("InputSystem::Update [" + id + "] JustReleased");
                }
            }
            else if (info.Active)
            {
                info.TimeActiveMs += elapsedMs;
            }
            else 
            {
                info.TimeActiveMs = 0;
            }
            mDispatchedInputs[id].ReloadData();
        }



    }
    
    public void Shutdown ()
    {
        mLogProvider = null;
    }
    #endregion

    public IUpdatable SetLogProvider (ILogProvider provider)
    {
        mLogProvider = provider;
        return this;
    }

    private void InternalAddRegisteredId(string id)
    {
        if (!mRegisteredInputs.ContainsKey(id))
        {
            // This is the first registered keycode for this id.
            InputInfo registeredInput = new InputInfo(null);
            mRegisteredInputs.Add(id, registeredInput);
            // And add the read-only version to mDispatchedInputs.
            mDispatchedInputs.Add(id, new InputInfo(registeredInput));
        }        
    }


}

} }
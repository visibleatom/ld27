namespace pax { namespace core {
public interface ILogProvider 
{
	void SetLogEnabled(bool enabled);
	void Log(object message);
	void Log(object message, UnityEngine.Object context);
}
}}
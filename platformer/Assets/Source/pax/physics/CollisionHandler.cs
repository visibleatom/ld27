﻿using UnityEngine;
using System.Collections;
namespace pax.physics 
{
    public struct Contact
    {
        Vector2 position;
        Vector2 normal;
        float penetration;
    }

    // structure to keep track of collision information
    public struct Manifold
    {
        PhysicsBody a;
        PhysicsBody b;
        int numContacts;
        Contact[] contacts;
    }

    // axis-aligned bounding box
    public struct BoundingBox
    {
        public Vector2 Min;
        public Vector2 Max;
    }

    public struct Circle
    {
        public float X { get { return Center.x; } set { Center.x = value;} }
        public float Y { get { return Center.y; } set { Center.y = value;} }

        public Vector2 Center;
        public float Radius;
    }   
                            

    public class CollisionHandler
    {

    }


}
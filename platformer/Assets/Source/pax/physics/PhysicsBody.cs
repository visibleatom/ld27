using System.Collections;
using System.Collections.Generic;
using pax.core;
using pax.math;

namespace pax.physics 
{

    public class PhysicsBody : IUpdatable , IQuadTreeNode
    {

        // debug-only? feature. I need to know where the hell these things are getting their bad data from.
        public string Id;


        public float X { get { return mCurrentPosition.X; }  set { mCurrentPosition.X = value; UpdateQuadTree(); }}
        public float Y { get { return mCurrentPosition.Y; }  set { mCurrentPosition.Y = value; UpdateQuadTree(); }}

        enum Type
        {
            CIRCLE,
            RECTANGLE
        };

        // List of forces, with the scale of 1 unit per 1000 ms
        // so a force of (0,-1), for instance, would increase velocity
        // by -1 every 1000 seconds.
        Dictionary<string, Vector2> mForces;


        // Current velocity. 1 unit per 1000 ms, so it will change 
        // position by 1 unit per second.
        Vector2 mCurrentVelocity;

        Vector2 mCurrentPosition;   
        Vector2 mPreviousPosition;   
        Type mType;

        Rect mRect;

        Vector2 mFrameForceContribution;
        PhysicsEngine mPhysicsEngine;
        QuadTree mQuadTree;

        private int mTotalTime;

        public PhysicsBody(PhysicsEngine engine)
        {
            mTotalTime = 0;
            mPhysicsEngine = engine;
            mCurrentPosition = new Vector2(0,0);
            mPreviousPosition = new Vector2(0,0);
            mCurrentVelocity = new Vector2(0,0);
            mRect = new Rect(0,0,0,0);
            mType = Type.RECTANGLE;
            mForces = new Dictionary<string, Vector2>();
            mFrameForceContribution = new Vector2(0,0);
        }


        public void SetDimensions(Rect newDims)
        {

            mRect = newDims;
            UpdateQuadTree();
        }

        public void Update(int elapsedMs)
        {
            mTotalTime += elapsedMs;
            //Debug.Log("elapsedMs : " +elapsedMs + " TOTAL : " + mTotalTime);
            mFrameForceContribution.X = 0;
            mFrameForceContribution.Y = 0;

            foreach(KeyValuePair<string, Vector2> pair in mForces)
            {
                Vector2 force = pair.Value;
                //Log("force " + pair.Key + " = " + force.ToString());
                mFrameForceContribution.X += force.X;// * elapsedMs;
                mFrameForceContribution.Y += force.Y;// * elapsedMs;
            }

            // Also, apply the deceleration force, based on the current velocity (we don't want to flip direction.)

            float deceleration = (mCurrentVelocity.X) * elapsedMs * 0.5f / 1000f;
            if ((mCurrentVelocity.X > 0 && deceleration > mCurrentVelocity.X)
              ||(mCurrentVelocity.X < 0 && deceleration < mCurrentVelocity.X))
            {
                deceleration = mCurrentVelocity.X;
            }

            //mFrameForceContribution.X -= deceleration;
            //Log ("Applying horizontal deceleration -(" + mCurrentVelocity.X + ") * (" + elapsedMs + ") * .05f = " + (-(mCurrentVelocity.X) * elapsedMs * 0.5f));
            //mFrameForceContribution.Y += -(mCurrentVelocity.Y) * elapsedMs * 0.5f;
            //mFrameForceContribution.z += -(mCurrentVelocity.z) * elapsedMs * 0.5f;

            //mFrameForceContribution.X;
            //mFrameForceContribution.Y;
            //mFrameForceContribution.z;
            //Log("mFrameForceContribution=" + mFrameForceContribution.ToString());

            // This is just in case we want to do anything weird/crazy
            // with the force contributions in this frame.

            mCurrentVelocity.X += mFrameForceContribution.X;
            mCurrentVelocity.Y += mFrameForceContribution.Y;
            Log("mCurrentVelocity=" + mCurrentVelocity.ToString());

            // Cast rays out in the direction of velocity, see if it hits anything?
            //PhysicsBody [] collisions = mPhysicsEngine.CastCollisionRay(mCurrentPosition, mCurrentVelocity);


            // At this point, our velocity might by CRAZY fast.

            // Do we have a maximum speed set? Clamp it.

            // Check whether we can go in this direction without collisions.
            // Since we don't want to be able to 'skip' an object due to extreme
            // speeds, we might need to break this down into smaller steps.
            // As long as we don't move farther than our entire body length,
            // we won't miss out on any collisions. The smaller the step, the safer
            // it is (although at that point, we might as well simply have updated
            // this with a smaller elapsedMs and get nicer curves just in case).

            if (!pax.util.Math.IsZero(mCurrentVelocity))
            {
                // Store previous position.
                mPreviousPosition.X = mCurrentPosition.X;
                mPreviousPosition.Y = mCurrentPosition.Y;

                mCurrentPosition.X += mCurrentVelocity.X * elapsedMs / 1000f;
                mCurrentPosition.Y += mCurrentVelocity.Y * elapsedMs / 1000f;

                UpdateQuadTree();
            }

        }

        public void SetSpeedX(float speed)
        {
            mCurrentVelocity.X = speed;
        }
        public void SetSpeedY(float speed)
        {
            mCurrentVelocity.Y = speed;
        }

        public void AddForce(string id, Vector2 force)
        {
            if (mForces.ContainsKey(id))
            {
                mForces[id] = force;

            }
            else
            {
                mForces.Add(id, force);
            }
            
        }

        public bool IsColliding(PhysicsBody other)
        {
            if (other.mType == Type.CIRCLE)
            {
                // TODO!
                return false;
            }
            return (this.mRect.MinX < other.mRect.MaxX && this.mRect.MaxX > other.mRect.MinX &&
                    this.mRect.MinY < other.mRect.MaxY && this.mRect.MaxY > other.mRect.MinY) ;
        }

        public void Shutdown()
        {

        }

        #region IQuadTreeNode
        public void SetQuadTree(QuadTree t)
        {
            mQuadTree = t;
        }
        public float GetWidth()
        {
            return mRect.Width;
        }
        public float GetHeight()
        {
            return mRect.Height;
        }
        public float GetX()
        {
            return mRect.X;
        }
        public float GetY()
        {
            return mRect.Y;
        }
        #endregion

        private void UpdateQuadTree()
        {
            if (mQuadTree != null)
            {
                mRect.X = mCurrentPosition.X;
                mRect.Y = mCurrentPosition.Y;
                mQuadTree.UpdatePosition(this);
            }
        }

        private void Log(string message)
        {
            UnityEngine.Debug.Log(message);
        }
    }

}
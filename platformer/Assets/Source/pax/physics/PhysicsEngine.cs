using UnityEngine;
using System.Collections;
using pax.core;

namespace pax.physics 
{

    public class PhysicsEngine
    {
        private QuadTree mBodies;
        private CollisionHandler mCollisionHandler;

        public PhysicsEngine()
        {
            // TODO - grab world area some other way.
            mBodies = new QuadTree(null, 0, new Rect(-1000,-1000, 2000, 2000));
            mCollisionHandler = new CollisionHandler();

        }

        public void RegisterBody(PhysicsBody body)
        {
            mBodies.Insert(body);
        }

        public PhysicsBody[] CastCollisionRay(Vector3 origin, Vector3 end)
        {

            return null;
        }

        public ArrayList GetCollisions(PhysicsBody body)
        {
            ArrayList possibleCollisions = mBodies.Retrieve(new ArrayList(), body);
            ArrayList collisions = new ArrayList();
            foreach(PhysicsBody other in possibleCollisions)
            {
                if (body != other && body.IsColliding(other))
                {
                    collisions.Add(other);
                }
            }
            return collisions;
        }
    }

}
﻿// Please avoid the 'using' keyword in any pax.util file.
// These are meant to be fairly self-supporting, and I would rather that we
// are VERY clear inline when we are referencing an outside function/class.

namespace pax.util
{
    public class Math
    {
        public const float EPSILON_ZERO = 0.0001f;

        public static bool IsZero(float a)
        {
        	return (a>0) ? (a<EPSILON_ZERO) : (a>-EPSILON_ZERO);
        }
        public static bool IsZero(pax.math.Vector2 a)
        {
            return (a.X < EPSILON_ZERO && a.Y < EPSILON_ZERO);
        }

        public static float sqrt(float a)
        {
            return UnityEngine.Mathf.Sqrt(a);
        }
    }
}

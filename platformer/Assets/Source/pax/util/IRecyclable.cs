﻿using UnityEngine;
using System.Collections;
namespace pax.util
{

    public interface IRecyclable<T>
    {
        T GetNewRecyclable();
    }
}
using System.Collections.Generic;
namespace pax.util
{
    public class ObjectPoolManager
    {
        public const int INVALID_ID = -1;
        public static ObjectPoolManager Instance { get { return sInstance; } }

        public static void CreateInstance()
        {
            sInstance = new ObjectPoolManager();
        }

        private static ObjectPoolManager sInstance;

        private ObjectPoolManager()
        {

        }

        public ObjectPool<T> Get<T>() 
        {
            return null;
        }

    }
}
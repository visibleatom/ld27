using System; // Exception
using System.Threading; // Mutex
using System.Collections;
using System.Collections.Generic;

namespace pax.util
{

    public class ObjectPool<K> 
    {
        private const int INVALID_ID = ObjectPoolManager.INVALID_ID;

        public interface DefaultDataCallback
        {
            K Callback();
        }
 
        protected class ObjectContainer //where K : IRecyclable<K>
        {
            public K Data;
            public bool InUse;

            public ObjectContainer(K data)
            {
                Data = data;
                InUse = false;
            }
        }

        protected List<ObjectContainer> mList;
        protected List<int> mAvailableIds;
        private static Mutex sMutex = new Mutex();

        DefaultDataCallback mCallback;
        
        public string DebugName;

        public ObjectPool(DefaultDataCallback callback, string debugName)
        {
            DebugName = debugName;
            mCallback = callback;
            mList = new List<ObjectContainer>();
            // TODO TODO TODO - this has to be thread-safe. HAS TO BE. Unity doesn't provide
            // access to System.Collections.Generic.SynchronizedCollection. If we have to,
            // we'll need to create our own thread-safe collection.
            mAvailableIds = new List<int>();
        }

        public K GetReservedData(int id)
        {
            // note; we could probably remove this in the future once we do enough testing.

            if (id < mList.Count)
            {
                if (mList[id].InUse == true)
                {
                    return mList[id].Data;
                }
                else
                {
//                    DebugUtil.Assert("GetReservedData: " + id + " is not in use. This means we didn't reserve the id?");        
                }
            }

//            DebugUtil.Assert("GetReservedData: " + id + " is null. We are SO going to crash :(");
            return default(K);
        }

        public void ReleaseData(int id)
        {
            sMutex.WaitOne();

            if (id < mList.Count)
            {
                mList[id].InUse = false;
                mAvailableIds.Add(id);
            }

            sMutex.ReleaseMutex();
        }

        // TODO - Trying to make this thread-safe
        public int ReserveDataId()
        {
            sMutex.WaitOne();
            int tempInt = INVALID_ID;
            int reservedIdx = INVALID_ID;
            ObjectContainer tmpContainer;
            int loopCounter = 0;
            
            while (reservedIdx == -1)
            {
                
                if (mAvailableIds.Count > 0)
                {
                    tempInt = mAvailableIds[0];
                    mAvailableIds.RemoveAt(0);
                }
    
                if (tempInt != INVALID_ID)
                {
                    reservedIdx = tempInt;

                    try
                    {
                        tmpContainer = mList[reservedIdx];
                        if (tmpContainer.InUse == true)
                        {
                            DebugUtil.Logd(DebugName + ": Container for " + reservedIdx + " was already in use");
                            //breakpoint. This is a problem
                            reservedIdx = INVALID_ID;
                        }
                        else
                        {
                            tmpContainer.InUse = true;
                        }
                    }
                    catch(Exception e)
                    {
                        DebugUtil.Assert(DebugName + ": " + e.Message);
                    }
    
                }
    
                if (reservedIdx == -1)
                {

                    mList.Add(new ObjectContainer((K)mCallback.Callback()));
                    
                    int newVal = mList.Count - 1;
                    
                    int addableVal = newVal;
                    
                    /*
                    if(mAvailableIds.Count > 0)
                    {
                        if(null == mAvailableIds[0])
                        {
                            mAvailableIds = new LinkedList<Integer>();
                        }
                    }
                    */
                    mAvailableIds.Add(addableVal);
                }
                loopCounter++;
                
                if(loopCounter > 4)
                {
                    int i = 0;
                    i++;
                }
                
                if (loopCounter > 50)
                {
//                    DebugUtil.Assert(DebugName + ": There is NO WAY that we would've gone through this loop more than once. This is a big problem.");
                }
            }

            sMutex.ReleaseMutex();
            return reservedIdx;
        }

    }

}